# -*- coding: utf-8 -*-
from gunicorn import util
from gunicorn.app.base import Application


class FixtureApplication(Application):

    def __init__(self, options=None):
        self.options = options
        self.usage = None
        self.callable = None
        self.prog = ''
        self.do_load_config()

    def init(self, *args):
        cfg = {}
        for k, v in self.options.items():
            if k.lower() in self.cfg.settings and v is not None:
                cfg[k.lower()] = v
        return cfg

    def load(self):
        return util.import_app("ws_checker:wsgi")


if __name__ == '__main__':
    options = {
        'bind': 'localhost:8080',
        'loglevel': 'debug'
    }

    FixtureApplication(options).run()