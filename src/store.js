import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native
import { persistStore, persistReducer } from 'redux-persist'
import rootReducer from './reducers'

const persistConfig = {
  key: 'root',
  storage,
}

const reducer = persistReducer(persistConfig, rootReducer)

const store  = createStore(reducer, {}, composeWithDevTools(
    applyMiddleware(thunkMiddleware)
    )
)

export default () => {
    let store = createStore(reducer,
        {},
        composeWithDevTools(applyMiddleware(thunkMiddleware)))
    let persistor = persistStore(store)
    return { store, persistor }
}