import React, { Component } from 'react'
import { connect } from 'react-redux'
import Navbar from '../Navbar'
import PropTypes from 'prop-types';
import { DataTableRedux as DataTable } from 'redux-remote-datatable';


function DataTableAuth(props){
    const isAuthenticated = props.props.isAuthenticated;
    if (isAuthenticated){

        return  <DataTable
          fields={{"Name": "official_name", "Birthday": "birthday", "Thomas ID": "thomas_id" }}
          ajax="http://localhost:8080/api/ws/"
          idField="url" />
    }
    else {
        return <div>Authentecate to view the table</div>
    }

}


class App extends Component {
  render() {
    const { dispatch, isAuthenticated, errorMessage } = this.props
    return (
          <div>
            <Navbar
              isAuthenticated={isAuthenticated}
              errorMessage={errorMessage}
              dispatch={dispatch}/>
            <DataTableAuth props={this.props}/>
          </div>

        )
  }
}


App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string,
}


// These props come from the application's
// state when it is started
function mapStateToProps(state) {

  const { auth } = state
  const { isAuthenticated, errorMessage } = auth

  return {
    isAuthenticated,
    errorMessage
  }
}

export default connect(mapStateToProps)(App)