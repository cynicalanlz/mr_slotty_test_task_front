import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import App from './components/containers/App'
import websiteCheker from './reducers'
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';
import { Route, IndexRoute } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom'
import { PersistGate } from 'redux-persist/es/integration/react'
import configureStore from './store'

const { store, persistor } = configureStore()


// const store  = createStore (websiteCheker, {}, composeWithDevTools(
//     applyMiddleware(thunkMiddleware)
//     )
// )
console.log(store)

render(
  <Provider store={store}>
    <PersistGate
      loading={null}
      persistor={persistor}
      >
      <Router>
          <Route path="/" component={App}/>
      </Router>
    </PersistGate>
  </Provider>,
  document.getElementById('root')
)